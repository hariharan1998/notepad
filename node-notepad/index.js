var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  mongoose = require('mongoose'),
  Task = require('./api/models/register.model'), //created model loading here
  register = require('./api/models/project.model'),
  bodyParser = require('body-parser');
  var cors = require('cors')
  
// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/nodepad'); 


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors())

var routes = require('./api/routes/register.routes'); //importing route
routes(app); //register the route
var register = require('./api/routes/project.routes');
register(app);

app.listen(port);


console.log('todo list RESTful API server started on: ' + port);