'use strict';
module.exports = function(app) {
  var todoList = require('../controller/project.controller');

  // todoList Routes
  app.route('/project')
    .get(todoList.list_all_tasks)
    .post(todoList.create_a_task);

    app.route('/getprojectbasedonuser')
     .post(todoList.get_projectlist)

  app.route('/project/:projectId')
    .get(todoList.read_a_task)
    .put(todoList.update_a_task)
    .delete(todoList.delete_a_task);
};