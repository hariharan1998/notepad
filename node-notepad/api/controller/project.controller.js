'use strict';


var mongoose = require('mongoose'),
  Task = mongoose.model('Project');

exports.list_all_tasks = function(req, res) {
  Task.find({}, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};




exports.create_a_task = function(req, res) {

    Task.findOne({name:req.body.projectname},function(err,result){
        if(err){
            res.json("something wentwrong")
        }else{
            if(result){
                res.json("Projectname already exist")
            }else{
                var new_task = new Task(req.body);
                new_task.save(function(err, task) {
                  if (err)
                    res.send(err);
                  res.json(task);
                });
            }
        }
    })
};

exports.get_projectlist = function(req,res){
    console.log(req.body.userid)
    Task.find({},function(err,result){
        // console.log(result)
        Task.find({ users:req.body.userid},function(err,resut1){
              console.log(resut1)
              if(err){
                  res.json("something went worng")
              }else{
                  res.json(resut1)
              }
          })
    })
}


exports.read_a_task = function(req, res) {
  Task.findById(req.params.projectId, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};


exports.update_a_task = function(req, res) {
  Task.findOneAndUpdate({_id: req.params.projectId}, req.body, {new: true}, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};


exports.delete_a_task = function(req, res) {


  Task.remove({
    _id: req.params.projectId
  }, function(err, task) {
    if (err)
      res.send(err);
    res.json({ message: 'Task successfully deleted' });
  });
};
